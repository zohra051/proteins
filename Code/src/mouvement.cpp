#include "mouvement.hpp"

mouvement::mouvement(int position,int acide, double gain):
position(position),acide(acide),gain(gain)
{}

double mouvement::getGain(){
	return gain;
}

int mouvement::getPosition()
{
	return position;
}

int mouvement::getAcide()
{
	return acide;
}

int mouvement::appliquer(vector<int> & chaine){
	int ancien_acide = chaine[position];
	chaine[position] = acide;
	return ancien_acide;
}

