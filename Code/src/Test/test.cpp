#include <CppUTest/CommandLineTestRunner.h>
#include "../fonction.hpp"
TEST_GROUP(FonctionObjective) { };
TEST_GROUP(FonctionEnergie_P_G) {};

TEST(FonctionObjective,energieAcide)
{
	string fichier = "../src/Test/result.txt";
	fonction f;
	f.lecture_fichier(fichier);
	CHECK(f.Energie_acide(0,3) == 38);
}
TEST(FonctionObjective,energieInteraction)
{
	string fichier = "../src/Test/result.txt";
	fonction f;
	f.lecture_fichier(fichier);
	CHECK(f.Energie_interaction(2,0,0,0) == 88);
	CHECK(f.Energie_interaction(0,0,2,0) == 88);
}
TEST(FonctionObjective,fonctionEntiere)
{
	string fichier = "../src/Test/result.txt";
	vector<int> tableau;
	tableau.push_back(2);
	tableau.push_back(3);
	tableau.push_back(1);
	fonction f;
	f.lecture_fichier(fichier);
	CHECK(f.Fonction(tableau) == 455);
}

TEST(FonctionEnergie_P_G, fonction_P_G)
{
	string fichier = "../src/Test/result.txt";
	vector<int> tableau;
	tableau.push_back(2);
	tableau.push_back(3);
	tableau.push_back(1);
	fonction f;
	f.lecture_fichier(fichier);

	CHECK(f.Energie_perdue_gagnee(1,tableau) == 245);
}
