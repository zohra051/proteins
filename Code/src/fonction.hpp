#ifndef FONCTION_HPP
#define FONCTION_HPP

#include <iostream>
#include <string>
#include <vector>


using namespace std;

class fonction
{
	private:
		/** \brief Taille de la protéine */
		int taille;
		/************************/
		/** \brief Tableau des acides possibles par position dans la chaine*/
		vector<int> choix;
		/************************/
		/** \brief Tableau des valeurs d'énergies pour chaques acides
		 *
		 * Tableau contenant un tableau de valeur d'acide pour chaque position
		 * */
		vector<vector<double>> tableau_acide;
		/************************/
		/** \brief Tableau des valeurs d'énergie d'interaction entre chaques acides */
		vector<vector<vector<vector<double>>>> tableau_interaction;

		vector<vector<int>> interaction_possible;

	public:
		/** \brief Constructeur vide */
		fonction();

		/** \brief Retourne l'énerige d'un acide
		 * \param[in] position Position de l'acide dans la protéine
		 * \param[in] acide Numéro de l'acide
		 * \return Valeur d'Energie de l'acide*/
		double Energie_acide(int position, int acide);

		/** \brief Retourne l'energie d'interaction entre deux acides
		 * \param[in] position_x Position de l'acide X dans la protéine
		 * \param[in] acide_x Numéro de l'acide X
		 * \param[in] position_y Position de l'acide Y dans la protéine
		 * \param[in] acide_y Numéro de l'acide Y
		 * \return Valeur d'interaction entre l'acide X et Y
		 * */
		double Energie_interaction(int position_x, int acide_x, int position_y, int acide_y);

		/** \brief Fonction Objective : F(x) = sum(E(X))+sum(sum(E(Xi,Xj)))
		 * \param[in] chaine Protéine
		 * \return Valeur de la fonction objective*/
		double Fonction(const vector<int> & chaine);

		/** \brief Retourne l'energie de l'acide en position donner + l'énergie de d'interaction entre tous les autres acides
		 * \param[in] position Position de l'acide dans la protéine
		 * \param[in] chaine Protéine
		 * \return Valeur d'energie propre et interaction d'un acide*/
		double Energie_Total_Acide(int position,const vector<int> & chaine);

		/** \brief Lecture du fichier d'instance */
		void lecture_fichier(string fichier);


		int getTaille();
		int getChoix(int i);
		vector<int> getInterractionPossible(int position);
};

#endif
