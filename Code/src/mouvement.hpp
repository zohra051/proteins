#ifndef MOUVEMENT_HPP
#define MOUVEMENT_HPP
#include <iostream>
#include <vector>

using namespace std;

class mouvement
{
	private:
		int position;
		int acide;
		double gain;

	public:
		mouvement(int position, int acide, double gain);
		double getGain();
		int getPosition();
		int getAcide();
		int appliquer(vector<int> & chaine);

};

#endif

