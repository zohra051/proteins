#include "fonction.hpp"
#include "rapidjson/document.h"
#include <string>
#include <iostream>
#include <fstream>
#include <limits>
using namespace rapidjson;

fonction::fonction()
{

}

double fonction :: Energie_acide(int position, int acide)
{
	return tableau_acide[position][acide];
}

double fonction :: Energie_interaction(int position_x, int acide_x, int position_y, int acide_y)
{
	/*Si l'acide X est avant l'acide Y dans la protéine
		 * On calcul l'interaction[X][Y]
	 * Sinon
		 * On calcul l'interacton[Y][X]*/
	if(position_x<position_y)
	{
		return tableau_interaction[position_x][acide_x][position_y][acide_y];
	}
	else
	{
		return tableau_interaction[position_y][acide_y][position_x][acide_x];
	}
}

double fonction::Energie_Total_Acide(int position,const vector<int> & chaine)
{
	/* Energie totale : Energie de l'acide passé en paramètre */
	double energie_totale = Energie_acide(position,chaine[position]);

	/* Pour chaque acide dans la protéine
		 * S'il ne correspond pas à la position passé en paramètre
			 * On somme l'énergie d'interaction à l'énergie totale

		* Retourne l'énergie totale :  */
	for(int i=0;i<interaction_possible[position].size();i++)
	{
		if(interaction_possible[position][i] != position)
		{
			int position_interraction = interaction_possible[position][i];
			energie_totale = energie_totale + Energie_interaction(position, chaine[position],position_interraction,chaine[position_interraction]);
		}
	}
	return energie_totale;
}

double fonction :: Fonction(const vector<int> & chaine)
{
	double sum=0.0;
	/*Calcul de toutes les énergies propres de chaque acide dans la protéine*/
	for(int i=0;i<chaine.size();i++)
	{
		sum += Energie_acide(i,chaine[i]);
	}
	/*Calcul de toutes les interactions des acides entre eux*/
	for(int i=0;i<chaine.size();i++)
	{
		for(int j=0;j<interaction_possible[i].size();j++)
		{
			//on vérifie que l'interraction se fait sur des acides qui sont plus loin dans la chaîne
			if(i<interaction_possible[i][j])
			{
				sum += Energie_interaction(i,chaine[i],interaction_possible[i][j],chaine[interaction_possible[i][j]]);

			}
		}
	}
	/*Energie propre + Energie interaction*/
	return sum;
}

void fonction::lecture_fichier(string fichier_entre)
{
	std::ifstream fichier(fichier_entre);
	std::string comment;
	std::getline(fichier,comment);

	Document document;
	std::string json((std::istreambuf_iterator<char>(fichier)),std::istreambuf_iterator<char>());

	fichier.close();
	document.Parse(json.c_str());




	//on récupère variables
	const Value& variables = document["variables"];
	taille =0;

	/*variables : itr-> name.GetString() : V1 / K2 / D3 /G4
	 * taille : 4
	 * second : "V0" "V1" / "K0" ...
	 * choix : 2 / 5 / 6 / 2
	*/
	//cout<<"variables:{"<<endl;
	for(Value::ConstMemberIterator itr = variables.MemberBegin(); itr != variables.MemberEnd();itr++)
	{
		/*V1 / K2 / D3 /G4*/
		//cout<<itr-> name.GetString()<<":[ "<<endl;
		taille++;

		/* "V0" "V1" / "K0" ... */
		const Value& second = variables[itr -> name.GetString()];
		/*for(int i=0 ; i< second.Size();i++){
			cout<<second[i].GetString()<<","<<endl;
		}
		cout<<"],"<<endl;*/
		choix.push_back(second.Size());
	}
	/*cout<<"}";
	for(int i=0 ; i< choix.size();i++){
		cout<<choix[i];
	}*/

	//on récupère les valeurs d'énergie et intéraction
	const Value& functions = document["functions"];
	tableau_acide.resize(taille);
	//cout<<"functions:{"<<endl;

	interaction_possible.resize(taille);

	//Mise à nu des tebleaux d'interactions
	tableau_interaction.resize(taille);
		for(int i=0;i<taille;i++)
		{
			tableau_interaction[i].resize(choix[i]);
			for(int j=0;j<choix[i];j++)
			{
				tableau_interaction[i][j].resize(taille);
				for(int k=i+1;k<taille;k++)
				{
					tableau_interaction[i][j][k].resize(choix[k]);
				}
			}
		}


	for(Value::ConstMemberIterator itr = functions.MemberBegin(); itr != functions.MemberEnd();itr++)
	{
		const Value& Energy = functions[itr-> name.GetString()];
		//cout<<itr->name.GetString()<<":{"<<endl;
		//SCOPE
		const Value& scope = Energy["scope"];
		const Value& costs = Energy["costs"];

		//Energie d'un acide
		if(scope.Size() == 1)
		{
			//cout<<"scope:["<<endl;
			string str = scope[0].GetString();
			//cout<<str<<endl;
			int position = stoi(str.substr(1))-1;
			//cout<<"],"<<endl<<"costs:["<<endl;
			for(SizeType i = 0;i<costs.Size();i++)
			{
				tableau_acide[position].push_back(costs[i].GetDouble());
				//cout<<costs[i].GetDouble()<<endl;
			}
			//cout<<"]"<<endl<<"},"<<endl;

		}
		//Enegie d'Interaction entre deux acides
		else if(scope.Size() == 2)
		{
			if(Energy.HasMember("defaultcost")==false)
			{
				//cout<<"scope:["<<endl;
				string str = scope[0].GetString();
				//cout<<str<<","<<endl;
				int position1 = stoi(str.substr(1))-1;
				str = scope[1].GetString();
				//cout<<str<<endl<<"],"<<endl;
				int position2 = stoi(str.substr(1))-1;
				int cpt =0;
				//cout<<"costs:["<<endl;
				for(int i=0;i<choix[position1];i++)
				{
					for(int j=0;j<choix[position2];j++)
					{
						tableau_interaction[position1][i][position2][j] = costs[cpt].GetDouble();
						//cout<<costs[cpt].GetDouble()<<endl;
						cpt++;

					}
				}
				interaction_possible[position1].push_back(position2);
				interaction_possible[position2].push_back(position1);
				//cout<<"]"<<endl<<"},"<<endl;
			}
			else
			{
				//DEFAULTCOST
				const Value& defaultcost = Energy["defaultcost"];

				//COST
				const Value& cost = Energy["costs"];
				//cout<<"scope:["<<endl;
				string str = scope[0].GetString();
				//cout<<str<<","<<endl;
				int position1 = stoi(str.substr(1))-1;
				str = scope[1].GetString();
				//cout<<str<<endl<<"],"<<endl;
				int position2 = stoi(str.substr(1))-1;
				/*cout<<"defaultcost:"<<defaultcost.GetDouble()<<endl;
				cout<<"costs:["<<endl;*/
				for(int i=0;i<choix[position1];i++)
				{
					for(int j=0;j<choix[position2];j++)
					{
						tableau_interaction[position1][i][position2][j] = defaultcost.GetDouble();
					}
				}
				int cpt =0;
				while(cpt < cost.Size())
				{
					tableau_interaction[position1][cost[cpt].GetDouble()][position2][cost[cpt+1].GetDouble()] = cost[cpt+2].GetDouble();
					/*cout<<cost[cpt].GetDouble()<<","<<endl;
					cout<<cost[cpt+1].GetDouble()<<","<<endl;
					cout<<cost[cpt+2].GetDouble()<<","<<endl;*/
					cpt = cpt +3;
				}
				interaction_possible[position1].push_back(position2);
				interaction_possible[position2].push_back(position1);
				//cout<<"]"<<endl<<"},"<<endl;
			}
		}
		/*else if(scope.Size() == 0)
		{
			cout<<"scope:[],"<<endl;
			cout<<"costs:["<<endl;
			cout<<"]"<<endl<<"},"<<endl;

		}*/


	}
}

int fonction:: getTaille()
{
	return taille;
}

int fonction:: getChoix(int i)
{
	return choix[i];
}


vector<int> fonction::getInterractionPossible(int position)
{
	return interaction_possible[position];
}










