#include <iostream>
#include <string>
#include "fonction.hpp"
#include "Solution.hpp"
#include <fstream>
using namespace std;

int main(int argc, char* argv[])
{

	string fichier = argv[1];
	Solution s(fichier);
	ofstream fich(argv[4],ios::out);
	for(int i=0;i<100;i++)
	{
		const clock_t begin_time = clock();
		int iter = s.Tabu_Search(atoi(argv[2]),atof(argv[3]));
		fich << s.getFitness()<<endl;
		cout<<s.getFitness()<<endl;
		s.reset();
	}	

	return 0;

}
