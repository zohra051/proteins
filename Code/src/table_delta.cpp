#include "table_delta.hpp"
#include <climits>
#include <float.h>

table_delta::table_delta()
{}

void table_delta::init (fonction * f,vector<int> * solution)
{
	this->f = f;
	this->solution = solution;
	/*donne la taille des tables */
	int taille = f->getTaille();
	table_calcul.resize(taille);
	table_variation.resize(taille);
	for(int i=0;i<taille;i++)
	{
		int choix = f->getChoix(i);
		table_calcul[i].resize(choix);
		table_variation[i].resize(choix);
	}

	/*initialise la table de calcul des énergie et des variations*/
	for(int i=0;i<taille;i++)
	{
		int energie_initiale = f->Energie_Total_Acide(i,*solution);
		int initiale = solution->at(i);
		for(int j=0;j<table_calcul[i].size();j++)
		{
			solution->at(i) = j;
			double energie_mouvement = f->Energie_Total_Acide(i,*solution);
			table_calcul[i][j] = energie_mouvement;
			table_variation[i][j] = energie_mouvement - energie_initiale;
		}
		solution->at(i) = initiale;
	}
}

void table_delta::reinit()
{
	for(int i=0;i<f->getTaille();i++)
	{
		int energie_initiale = f->Energie_Total_Acide(i,*solution);
		int initiale = solution->at(i);
		for(int j=0;j<table_calcul[i].size();j++)
		{
			solution->at(i) = j;
			double energie_mouvement = f->Energie_Total_Acide(i,*solution);
			table_calcul[i][j] = energie_mouvement;
			table_variation[i][j] = energie_mouvement - energie_initiale;
		}
		solution->at(i) = initiale;
	}
}

mouvement table_delta::meilleur_mouvement()
{
	int i_best;
	int j_best;
	double best = DBL_MAX;
	for(int i=0;i<table_variation.size();++i)
	{
		for(int j=0;j<table_variation[i].size();j++)
		{
			if(table_variation[i][j] < best && table_variation[i][j] != 0)
			{
				i_best = i;
				j_best = j;
				best = table_variation[i][j];
			}
		}
	}
	return mouvement(i_best,j_best,best);
}

mouvement table_delta::meilleur_mouvement_nontabu(tabu_list tabu)
{
	int i_best;
	int j_best;
	double best = DBL_MAX;
	for(int i:tabu.getPositionNonTabu())
	{
		for(int j=0;j<table_variation[i].size();j++)
		{
			if(table_variation[i][j] < best && table_variation[i][j] != 0)
			{
				i_best = i;
				j_best = j;
				best = table_variation[i][j];
			}
		}
	}
	return mouvement(i_best,j_best,best);
}

void table_delta::update(int ancien_acide,mouvement& mouv)
{
	int position = mouv.getPosition();
	int nouveau_acide = mouv.getAcide();

	//maj de la variation de la position du mouvement
	for(int i=0;i< f->getChoix(position);i++)
	{
		table_variation[position][i] -= mouv.getGain();
	}
	//maj des valeur d'énergie et des variations des interrractions
	for(int inter_pos : f->getInterractionPossible(position))
	{
		if(inter_pos != position)
		{
			int acide_solution = solution->at(inter_pos);
			table_calcul[inter_pos][acide_solution] += (f->Energie_interaction(inter_pos,acide_solution,position,nouveau_acide) - f->Energie_interaction(inter_pos,acide_solution,position,ancien_acide));
			for(int inter_acide=0;inter_acide < f->getChoix(inter_pos);inter_acide++)
			{
				if(inter_acide != acide_solution)
				{
					double dif_energie= f->Energie_interaction(inter_pos,inter_acide,position,nouveau_acide) - f->Energie_interaction(inter_pos,inter_acide,position,ancien_acide);
					table_calcul[inter_pos][inter_acide] += dif_energie;
					table_variation[inter_pos][inter_acide] = table_calcul[inter_pos][inter_acide] - table_calcul[inter_pos][acide_solution];
				}
		}
		}
	}
}
