#ifndef TABLE_DELTA_HPP
#define TABLE_DELTA_HPP
#include "fonction.hpp"
#include "tabu_list.hpp"
#include "mouvement.hpp"
#include <vector>


class table_delta{
	private:
		vector<vector<double>> table_calcul;
		vector<vector<double>> table_variation;
		fonction * f;
		vector<int> * solution;
	public:
		table_delta();
		void init (fonction * f,vector<int> * solution);
		mouvement meilleur_mouvement ();
		void update(int ancien_acide,mouvement& mouv);
		void reinit();
		mouvement meilleur_mouvement_nontabu(tabu_list tabu);
};

#endif
