#include "tabu_list.hpp"
#include <climits>
tabu_list::tabu_list(int taille,int temp_tabu):
taille(taille),iteration(0),temp_tabu(temp_tabu)
{
	list_tabu.resize(taille,0);
}

void tabu_list::next_iter()
{
	iteration++;
	position_non_tabu.clear();
	bool tous_tabu = true;
	for(int i=0;i<taille;i++)
	{
		if(list_tabu[i] < iteration)
		{
			position_non_tabu.push_back(i);
			tous_tabu = false;
		}
	}
	if(tous_tabu)
	{
		//on rend non tabou un mouvement aléatoire
		/**/
		int non_tabou = rand()%taille;
		list_tabu[non_tabou] = iteration-1;
		position_non_tabu.push_back(non_tabou);
		/**/
		//augmentation de l'itération jusqua ce qu'un mouvement ne soit plus tabou
		/*
		int min = INT_MAX;
		for(int i=0;i<taille;i++)
		{
			if(min > list_tabu[i])
				min = list_tabu[i];
		}
		iteration = min;
		next_iter();
		*/
	}
}

void tabu_list::reset()
{
	for(int i=0;i<taille;i++)
	{
		list_tabu[i] = iteration;
	}
}

bool tabu_list::isTabu(mouvement mouv)
{
	int position = mouv.getPosition();
	if(list_tabu[position] >= iteration)
		return true;
	return false;
}

void tabu_list::tabu(mouvement mouv)
{
	int position = mouv.getPosition();
	list_tabu[position] = iteration + rand()%temp_tabu +1;
}

vector<int> tabu_list::getPositionNonTabu()
{
	return position_non_tabu;
}
