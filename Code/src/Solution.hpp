#ifndef SOLUTION_HPP
#define SOLUTION_HPP
#include "fonction.hpp"
#include "table_delta.hpp"
#include <iostream>
#include <array>

using namespace std;

class Solution
{
	private:
		/** \brief Solution courante*/
		vector<int> solution;
		/** \brief Fonction Objective */
		fonction f;

		table_delta tableDelta;

	public:
		/** \brief Génère une solution aléatoire après une lecture de fichier
		 * \param[in] fichier Fichier d'instance */
		Solution(string fichier);
		/** \brief Affichage d'une solution*/
		void print_Solution();
		/** \brief Heuristique : Hill Climber */
		int Hill_Climbing();

		int Hill_Climbing_v2();

		int Tabu_Search(int temp_tabu,float temp_execution);

		void reset();

		void printFitness();

		double getFitness();

};



#endif
