#include "Solution.hpp"
#include "fonction.hpp"
#include "table_delta.hpp"
#include "tabu_list.hpp"
#include <stdlib.h>
#include <time.h>
#include <vector>


Solution::Solution(string fichier)
{
	srand (time(NULL));
	/* Lecture du fichier d'instance*/
	cout<<"Lecture fichier ..."<<endl;
	f.lecture_fichier(fichier);

	cout<<"initialisation ..."<<endl;
	/*Génère une solution aléatoire*/
	for(int i=0;i<f.getTaille();i++)
	{
		solution.push_back(rand()%f.getChoix(i));
	}
	tableDelta.init(&f,&solution);

}

void Solution::reset()
{
	for(int i=0;i<f.getTaille();i++)
	{
		solution[i] = rand()%f.getChoix(i);
	}
	tableDelta.reinit();
}

void Solution :: print_Solution()
{
	for(int i=0;i<solution.size();i++)
	{
		cout<<solution[i]<< " ";
	}
	cout<<endl;
	cout<<f.Fonction(solution);
	cout<<endl;
}

void Solution::printFitness()
{
	cout<<f.Fonction(solution);
}

double Solution::getFitness()
{
	return f.Fonction(solution);
}

int Solution:: Hill_Climbing()
{
	cout<<"Hill climber ...";
	bool flag = true;
	int cpt = 0;
	while(flag == true)
	{
		cpt++;
		flag=false;
		int minimum = 0,eval = 0;
		vector<int> new_solution;
		vector<int> energie_debut;

		/* Evaluation de la solution initiale */
		for(int i=0;i<solution.size();i++)
		{
			energie_debut.push_back(f.Energie_Total_Acide(i,solution));
		}


		/*Itération jusqu'a trouver une meilleur solution */
		for(int i=0;i<solution.size();i++)
		{
			int depart = solution[i];
			for(int j=0;j<f.getChoix(i);j++)
			{
				if(j != depart)
				{
					eval = - energie_debut[i];
					solution[i] = j;
					eval = eval + f.Energie_Total_Acide(i,solution);
					if(eval < minimum)
					{
						flag = true;
						minimum = eval;
						new_solution = solution;
					}

				}
			}
			solution[i]=depart;
		}
		/*Nouvelle Solution*/
		if(flag == true)
			solution=new_solution;

	}
	return cpt;
}

int Solution::Hill_Climbing_v2()
{
	bool flag = true;
	int cpt = 0;
	while(flag == true)
	{
		cpt++;
		//recherche du meilleur delta
		mouvement meilleur_mouv = tableDelta.meilleur_mouvement();
		//si ce mouvement permet d'améliorer la fitness (gain < 0)
		if(meilleur_mouv.getGain() < 0)
		{
			//on applique le mouvement
			int ancien_acide = meilleur_mouv.appliquer(solution);
			//et on met a jour les tables
			tableDelta.update(ancien_acide,meilleur_mouv);
		}
		//sinon fin de la recherche
		else
		{
			flag = false;
		}
	}
	return cpt;
}

int Solution::Tabu_Search(int temp_tabu,float temp_execution)
{
	bool flag = true;
	int cpt = 0;
	tabu_list tabuList(solution.size(),temp_tabu);
	double eval = f.Fonction(solution);
	double best_eval = eval;
	vector<int> best_solution = solution;
	float reset_timer=0.0f;
	float iteration_begin;
	const clock_t begin_time = clock();
	while(flag == true)
	{
		iteration_begin = float( clock () - begin_time ) /  CLOCKS_PER_SEC;
		cpt++;
		tabuList.next_iter();
		//cherche le meilleur mouvement
		mouvement meilleur_mouv = tableDelta.meilleur_mouvement();
		//test si le mouvement est tabu
		if(tabuList.isTabu(meilleur_mouv))
		{
			//si il l'est, vérification du critère d'aspiration
			//si le mouvement permet d'avoir la meilleur solution jamais rencontré
			if(eval + meilleur_mouv.getGain() < best_eval)
			{
				//on réalise ce mouvement
				int ancien_acide = meilleur_mouv.appliquer(solution);
				tableDelta.update(ancien_acide,meilleur_mouv);
				eval += meilleur_mouv.getGain();
				best_eval = eval;
				best_solution = solution;
				reset_timer = 0.0f;
			}
			//sinon, on cherche un mouvement non tabu
			else
			{
				meilleur_mouv = tableDelta.meilleur_mouvement_nontabu(tabuList);
				int ancien_acide = meilleur_mouv.appliquer(solution);
				tableDelta.update(ancien_acide,meilleur_mouv);
				eval += meilleur_mouv.getGain();
				if(eval < best_eval)
				{
					best_eval = eval;
					best_solution = solution;
					reset_timer = 0.0f;
				}
				//on ajoute ce mouvement dans la list tabu
				tabuList.tabu(meilleur_mouv);
			}
		}
		//si il n'est pas tabu on peut le réaliser sans problème
		else
		{
			int ancien_acide = meilleur_mouv.appliquer(solution);
			tableDelta.update(ancien_acide,meilleur_mouv);
			eval += meilleur_mouv.getGain();
			if(eval < best_eval)
			{
				best_eval = eval;
				best_solution = solution;
				reset_timer = 0.0f;
			}
			//on ajoute ce mouvement dans la list tabu
			tabuList.tabu(meilleur_mouv);
		}
		reset_timer += (float( clock () - begin_time ) /  CLOCKS_PER_SEC) - iteration_begin;
		if(reset_timer > 7.0f)
		{
			reset();
			tabuList.reset();
			reset_timer = 0.0f;
		}
		if(float( clock () - begin_time ) /  CLOCKS_PER_SEC > temp_execution )
			flag = false;
	}
	solution = best_solution;
	return cpt;
}
