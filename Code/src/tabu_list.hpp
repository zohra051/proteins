#ifndef TABU_LIST_HPP
#define TABU_LIST_HPP
#include <vector>
#include "mouvement.hpp"

using namespace std;

class tabu_list{
	private:
		int taille;
		int temp_tabu;
		vector <int> list_tabu;
		vector <int> position_non_tabu;
		int iteration;
	public:
		tabu_list(int taille,int temp_tabu);
		void next_iter();
		bool isTabu(mouvement mouv);
		void tabu(mouvement mouv);
		vector<int> getPositionNonTabu();
		void reset();
};

#endif
